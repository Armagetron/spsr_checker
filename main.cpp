#include <iostream>
#include <string>

#define BIT(x, n) ((x >> n) & 1)

int main(int argc, char *argv[]) {
    if(argc < 2) {
        std::cout << "Please supply a spsr" << std::endl;
        return -1;
    }

    std::string szSpsr = argv[1];
    uint64_t  spsr = std::stoul(szSpsr, nullptr, 16);

    std::cout << spsr << std::endl;
    std::cout << "Negative result: " << BIT(spsr, 31) << std::endl;
    std::cout << "Zero result: " << BIT(spsr, 30) << std::endl;
    std::cout << "Carry out: " << BIT(spsr, 29) << std::endl;
    std::cout << "Overflow: " << BIT(spsr, 28) << std::endl;
    std::cout << "Software step: " << BIT(spsr, 21) << std::endl;
    std::cout << "Illegal Execution: " << BIT(spsr, 20) << std::endl;
    std::cout << "Debug: " << BIT(spsr, 9) << std::endl;
    std::cout << "System error: " << BIT(spsr, 8) << std::endl;
    std::cout << "IRQ: " << BIT(spsr, 7) << std::endl;
    std::cout << "FIQ: " << BIT(spsr, 6) << std::endl;
    std::cout << "Execution state: " << BIT(spsr, 4) << std::endl;
    std::cout << "Exception level: " << (spsr & 0xF) << std::endl;
    return 0;
}
cmake_minimum_required(VERSION 3.13)
project(spsr_checker)

set(CMAKE_CXX_STANDARD 17)

add_executable(spsr_checker main.cpp)